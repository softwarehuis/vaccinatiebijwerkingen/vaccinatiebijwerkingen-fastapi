import json

from fastapi.testclient import TestClient
from faker import Faker
from src import api

client = TestClient(api)
fake = Faker()

email = fake.ascii_email()
token = "000000"


class TestTokens(object):
    def test_post_token(self):
        global token
        json_obj = {
            "name": fake.name(),
            "email": email,
            "phone": fake.phone_number(),
            "professional": fake.boolean(),
            "profession": fake.sentence()
        }

        response = client.post(
            "/api/v1/token",
            headers={"origin": "test-suite"},
            data=json.dumps(json_obj),
        )
        assert response.json()['token'] is not None
        token = response.json()['token']
        assert response.status_code == 200

    def test_post_verify(self):
        json_obj = {
            "email": email,
            "token": token
        }

        response = client.post(
            "/api/v1/verify",
            data=json.dumps(json_obj),
        )
        assert response.json()['status'] == "OK"
        assert response.json()['message'] == "Token verified"
        assert response.status_code == 200
