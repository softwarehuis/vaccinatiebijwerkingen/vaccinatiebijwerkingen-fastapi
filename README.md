# vaccinatiebijwerkingen-api
An API to get remote data, stored in json files or cached in a database, for vaccinatiebijwerkingen frontend

## How to start

### Development:

- Create a Python 3 virtual env named `venv` and activate it.
- Install requirements inside your virtual env with `python -m pip install -r requirements.txt`
- Set up and customize the environment variabels, see below
- Start application with `python service.py`

### Production {Not yet implemented):

- Build Docker image (see Dockerfile)
- Create container with the required environments variabels (see below, customize them)
- Start container, the application will be exported under port 8000


## Handy endpoints
* [API homepage](http://localhost:8000/)
* [OpenApi docs](http://localhost:8000/docs) openapi interface useable for testing endpoints

## Environment variables
Set the following environment variabels before starting the application.

* <del>`DATABASE_URL=postgresql://user:pass@127.0.0.1/yourdatabase`</del>
* BACKEND_ORIGINS=http://localhost,http://localhost:4200,http://localhost:3000
