import json
import os

from pydantic import BaseSettings


class Settings(BaseSettings):
    DATABASE_URL = os.environ.get(
        'DATABASE_URL', "sqlite://")
    USERS = json.loads(os.environ.get('USERS', '{"demo": "test12345"}'))
    BACKEND_ORIGINS: str = os.environ.get(
        'BACKEND_ORIGINS', "http://localhost,http://localhost:4200,http://localhost:3000")


settings = Settings()
