// Please keep the countries sorted
export type CountryCode =
    | 'ar'
    | 'au'
    | 'bd'
    | 'br'
    | 'ca'
    | 'cl'
    | 'co'
    | 'de'
    | 'cz'
    | 'dk'
    | 'en'
    | 'es'
    | 'fr'
    | 'id'
    | 'in'
    | 'it'
    | 'lt'
    | 'mt'
    | 'mx'
    | 'my'
    | 'ng'
    | 'nl'
    | 'no'
    | 'np'
    | 'ph'
    | 'pt'
    | 'ro'
    | 'se'
    | 'sg'
    | 'sk'
    | 'tr'
    | 'ua'
    | 'us';

// Please keep locales sorted
export type Locale =
    | 'bd'
    | 'cs-CZ'
    | 'de-DE'
    | 'dk'
    | 'en'
    | 'en-AU'
    | 'en-CA'
    | 'en-IN'
    | 'en-MT'
    | 'en-MY'
    | 'en-NE'
    | 'en-NG'
    | 'en-PH'
    | 'en-SG'
    | 'en-US'
    | 'es-AR'
    | 'es-CL'
    | 'es-CO'
    | 'es-ES'
    | 'es-MX'
    | 'fr-FR'
    | 'id-ID'
    | 'it'
    | 'lt'
    | 'ms-MY'
    | 'ne'
    | 'nl'
    | 'no'
    | 'pt-BR'
    | 'pt-PT'
    | 'ro-RO'
    | 'se'
    | 'sk'
    | 'tr'
    | 'uk-UA'
    | 'zh-MY';

type LocaleToFlagMap = {
    [locale in Locale]: string;
};

export const localeToFlagMap: LocaleToFlagMap = {
    bd: 'bd',
    'cs-CZ': 'cz',
    'de-DE': 'de',
    dk: 'dk',
    en: 'gb',
    'en-AU': 'au',
    'en-CA': 'ca',
    'en-IN': 'gb',
    'en-MT': 'gb',
    'en-MY': 'gb',
    'en-NE': 'us',
    'en-NG': 'ng',
    'en-PH': 'ph',
    'en-SG': 'gb',
    'en-US': 'us',
    'es-AR': 'ar',
    'es-CL': 'cl',
    'es-CO': 'co',
    'es-ES': 'es',
    'es-MX': 'mx',
    'fr-FR': 'fr',
    'id-ID': 'id',
    it: 'it',
    lt: 'lt',
    'ms-MY': 'my',
    ne: 'np',
    nl: 'nl',
    no: 'no',
    'pt-BR': 'br',
    'pt-PT': 'pt',
    'ro-RO': 'ro',
    se: 'se',
    sk: 'sk',
    tr: 'tr',
    'uk-UA': 'ua',
    'zh-MY': 'cn'
};

export const localeToFlag = (locale: Locale): string => localeToFlagMap[locale];
export interface Coordinate {
    lat: number;
    lon: number;
}

export interface Municipality {
    name: string;
    population: string;
    postalCodes?: string[];
}

export interface Config {
    BASE_URL: string;
    COUNTRY_CODE: CountryCode;
    DB_PATH: string;
    LOCALE: Locale;
    MAP_CENTER: Coordinate;
    MAP_MAX_ZOOM: number;
    MAP_ZOOM: number;
    PASSCODE_LENGTH: number;
    RATE_LIMIT_COUNT: number;
    RATE_LIMIT_WINDOW: number;
    REDIRECT_TO_GOVERNMENT: boolean;
    SUPPORTED_LOCALES: Locale[];
    THOUSAND_SEPARATOR: string;
    ZIP_GUIDE: boolean;
    ZIP_PATTERN: string;
    ZIP_PLACEHOLDER: string;
}

export interface CountrySpecificTexts {
    CONTACT_EMAIL: string;
    COUNTRY_NAME: string;
    FIND_ZIP_CODE_URL?: string;
    LINK_TO_NATIONAL_HEALTH_SERVICES: string;
    TWITTER_NAME?: string;
}

// export function municipalitiesToCascader (country: CountryCode) {
//     const municipalitiesRaw = require('./' + country + '/municipalities.json')
//     let out = []
//     for (const municipality in municipalitiesRaw) {
//         let children: {label: string, value: string}[] = []
//         municipalitiesRaw[municipality].postalCodes.forEach((c: string, i: number) => {
//             children.push({
//                 "label": c,
//                 "value": c
//             })
//         })
//         out.push({
//             "label": municipality,
//             "value": municipality,
//             "children": children
//         })
//     }
//     return out
// }