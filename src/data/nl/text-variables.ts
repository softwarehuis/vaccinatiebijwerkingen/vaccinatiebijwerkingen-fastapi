import { CountrySpecificTexts } from '../types';

export const countrySpecificTexts: CountrySpecificTexts = {
  CONTACT_EMAIL: 'contact@vaccinatiebijwerkingen.nl',
  COUNTRY_NAME: 'Nederland',
  LINK_TO_NATIONAL_HEALTH_SERVICES: 'https://lareb.nl'
};
