from datetime import datetime
from typing import Optional, List
from uuid import UUID

from pydantic import BaseModel, EmailStr


class Report(BaseModel):
    key: UUID
    name: str
    area: str
    deceased: bool
    autopsy: bool
    medical: bool
    age: str
    reasons: List[str]


class Reporter(BaseModel):
    name: str
    email: Optional[EmailStr]
    phone: Optional[str] = None
    professional: bool = False
    profession: Optional[str] = None


class User(Reporter):
    id: Optional[int]
    ip_address: str
    token_hashed: str
    token_expires_on: datetime
    is_active: bool

    class Config:
        orm_mode = True


class Verification(BaseModel):
    email: EmailStr
    token: str


class Questionaire(BaseModel):
    reporter: Reporter
    reports: List[Report]


class Municipality(BaseModel):
    postal_codes: List[str]


class Coordinate(BaseModel):
    lat: float
    lon: float


class CasesObject(BaseModel):
    source: str
    count: int


class MedicalSubTopic(BaseModel):
    name: str
    description: Optional[str]


class MedicalTopic(BaseModel):
    name: str
    category: Optional[str]
    distinction: Optional[str]
    description: Optional[str]
    cases: Optional[List[CasesObject]]
    subtopics: Optional[List[MedicalSubTopic]]


class Medical(BaseModel):
    topics: List[MedicalTopic]
