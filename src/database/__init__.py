from datetime import datetime

from sqlalchemy import create_engine, Column, Integer, String, Boolean, DateTime
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, Session
from ..models import User
SQLALCHEMY_DATABASE_URL = "sqlite:///./persist/vaccinatiebijwerkingen.db"

engine = create_engine(
    SQLALCHEMY_DATABASE_URL, connect_args={"check_same_thread": False}
)
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

Base = declarative_base()


class DbUser(Base):
    __tablename__ = "users"

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String, index=True)
    email = Column(String, unique=True, index=True)
    phone = Column(String, unique=True, index=True)
    professional = Column(Boolean, default=False)
    profession = Column(String, default=None)
    ip_address = Column(String)
    token_hashed = Column(String)
    token_expires_on = Column(DateTime)
    is_active = Column(Boolean, default=True)
    session_token = Column(String, default=None)


class DbUserLog(Base):
    __tablename__ = "user_log"
    id = Column(Integer, primary_key=True, index=True)
    user_id = Column(Integer, index=True)
    action = Column(String)
    action_datetime = Column(DateTime)
    action_ip_address = Column(String)


Base.metadata.create_all(bind=engine)


def get_user_by_email(db: Session, email: str):
    return db.query(DbUser).filter(DbUser.email == email).first()


def create_log_entry(db: Session, db_user: DbUser, message: str):
    db_log_entry = DbUserLog(
        user_id=db_user.id,
        action=message,
        action_datetime=datetime.now(),
        action_ip_address=db_user.ip_address
    )

    db.add(db_log_entry)
    db.commit()
    db.refresh(db_log_entry)
    return db_log_entry


def create_user(db: Session, user: User):
    db_user = DbUser(
        name=user.name,
        email=user.email,
        phone=user.phone,
        professional=user.professional,
        profession=user.profession,
        ip_address=user.ip_address,
        token_hashed=user.token_hashed,
        token_expires_on=user.token_expires_on,
        is_active=user.is_active
    )

    db.add(db_user)
    db.commit()
    db.refresh(db_user)
    create_log_entry(db, db_user, "created")

    return db_user
