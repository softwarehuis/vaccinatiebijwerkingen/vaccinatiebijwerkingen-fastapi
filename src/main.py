from fastapi import FastAPI, Request, Response
from fastapi.openapi.utils import get_openapi
from sqlalchemy.exc import DatabaseError
from starlette.middleware.cors import CORSMiddleware

from . import routes, database
from .settings import settings

api = FastAPI()


def custom_openapi():
    if api.openapi_schema:
        return api.openapi_schema
    openapi_schema = get_openapi(
        title="Vaccinatiebijwerkingen-api",
        version="0.0.1",
        description="This is the API for vaccinatiebijwerkingen",
        routes=api.routes,
    )
    api.openapi_schema = openapi_schema
    return api.openapi_schema


@api.middleware("http")
async def db_session_middleware(request: Request, call_next):
    try:
        request.state.db = database.SessionLocal()
        response = await call_next(request)
    except DatabaseError:
        return Response("Internal server error", status_code=500)
    finally:
        request.state.db.close()
    return response


# Dependency
def get_db(request: Request):
    return request.state.db


api.add_middleware(
    CORSMiddleware,
    allow_origins=settings.BACKEND_ORIGINS.split(','),
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

# Routers moved to individual files in the ./routers folder
api.include_router(routes.router)
api.openapi = custom_openapi
