import datetime
import hashlib
import json
import os
import random
import secrets
import smtplib
import uuid
from email.mime.text import MIMEText
from typing import List
from urllib.parse import urlparse

from fastapi import APIRouter, HTTPException, Request, Depends
from fastapi.responses import JSONResponse
from sqlalchemy.orm import Session

from ..database import create_user, SessionLocal, get_user_by_email, DbUser
from ..models import Medical, Reporter, User, Verification

router = APIRouter()


def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


def hash_token(token: str) -> str:
    """ Hash a token for storage in the database """
    salt = uuid.uuid4().hex
    return hashlib.sha256(salt.encode() + token.encode()).hexdigest() + ':' + salt


def check_token(hashed: str, plain: str) -> bool:
    """ Check if a token matches the hashed token """

    hashed_token, salt = hashed.split(':')

    print(hashed_token)
    print(salt)

    print(plain)
    print(hashlib.sha256(salt.encode() + plain.encode()).hexdigest())
    return hashed_token == hashlib.sha256(salt.encode() + plain.encode()).hexdigest()


@router.get(
    "/api/v1/{country}/municipality/{name}.json",
    response_model_exclude_none=True,
    response_model_by_alias=True
)
def get_municipality(country: str, name: str):
    """ Get information for the given municipality"""
    try:
        if 1 < len(country) > 2:
            raise HTTPException(
                status_code=400,
                detail=f"code for {country} could be 2 letter ISO code"
            )
        with open(os.path.join('src', 'data', country.lower(), 'municipalities.json'), 'r') as _source:
            data = _source.read()
            obj = json.loads(data)
            for key, item in obj.items():
                if key.lower() == name.lower():
                    return {
                        "municipality": key.capitalize(),
                        "postal_codes": item['postalCodes']
                    }
        raise HTTPException(
            status_code=400,
            detail=f"municipality {name} for country {country} could not be found"
        )
    except FileNotFoundError:
        raise HTTPException(
            status_code=400,
            detail=f"country {country} could not be found"
        )


@router.get("/api/v1/{country}/municipalities.json",
            response_model_exclude_none=True,
            response_model_by_alias=True)
def get_municipalities(country: str):
    """ Get all municipalities for the given country"""
    try:
        if 1 < len(country) > 2:
            raise HTTPException(
                status_code=400,
                detail=f"code for {country} could be 2 letter ISO code"
            )
        with open(os.path.join('src', 'data', country.lower(), 'municipalities.json'), 'r') as _source:
            data = _source.read()
            obj = json.loads(data)
            out: List[str] = []
            for key, item in obj.items():
                if key != "":
                    out.append(key)
            out.sort()
            return out
    except FileNotFoundError:
        raise HTTPException(status_code=400, detail=f"country {country} could not be found")


@router.get("/api/v1/{country}/topics.json",
            response_model_exclude_none=True,
            response_model_by_alias=True)
def get_medical_topics(country: str):
    """Get a list of all the medical topics for the given country code"""
    try:
        if 1 < len(country) > 2:
            raise HTTPException(
                status_code=400,
                detail=f"code for {country} could be 2 letter ISO code"
            )
        with open(os.path.join('src', 'data', country.lower(), 'medical.json'), 'r') as _source:
            data = _source.read()
            obj = json.loads(data)
            out: List[str] = []
            for item in obj['topics']:
                print(item)
                if "name" in item:
                    out.append(item["name"])
            return out
    except FileNotFoundError:
        raise HTTPException(status_code=400, detail=f"country {country} could not be found")


@router.get("/api/v1/{country}/search/topics.json",
            response_model_exclude_none=True,
            response_model_by_alias=True)
def search_medical_topics(country: str, q: str):
    """Get a list of specific medical topics for the given country code and search string"""
    try:
        if 1 < len(country) > 2:
            raise HTTPException(
                status_code=400,
                detail=f"code for {country} could be 2 letter ISO code"
            )
        with open(os.path.join('src', 'data', country.lower(), 'medical.json'), 'r') as _source:
            data = _source.read()
            obj = json.loads(data)
            out: List[str] = []
            for item in obj['topics']:
                if "name" in item:
                    if q in item["name"]:
                        out.append(item["name"])
                if "description" in item:
                    if q in item["description"]:
                        out.append(item["name"])
            return list(set(out))  # Makes the returned values unique
    except FileNotFoundError:
        raise HTTPException(
            status_code=400,
            detail=f"country {country} could not be found"
        )


@router.get("/api/v1/{country}/medical.json",
            response_model=Medical,
            response_model_exclude_none=True,
            response_model_by_alias=True)
def get_medical(country: str):
    """Get a the raw medical file for the given country code"""
    try:
        if 1 < len(country) > 2:
            raise HTTPException(
                status_code=400,
                detail=f"code for {country} could be 2 letter ISO code"
            )
        with open(os.path.join('src', 'data', country.lower(), 'medical.json'), 'r') as _source:
            data = _source.read()
            obj = json.loads(data)
            return obj
    except FileNotFoundError:
        raise HTTPException(
            status_code=400,
            detail=f"country {country} could not be found"
        )


@router.post("/api/v1/token")
def post_token(reporter: Reporter, request: Request, db: Session = Depends(get_db)):
    """When receiving an email, send a token to the email address and wait for the return."""
    r = random.randint(111111, 999999)
    plain_token = "%06d" % r
    domain = urlparse(request.headers['origin']).netloc.split(":")[0]
    print(domain)
    hashed_token = hash_token(plain_token)
    try:
        user = User(
            name=reporter.name,
            email=reporter.email,
            phone=reporter.phone,
            professional=reporter.professional,
            profession=reporter.profession,
            ip_address=request.client.host,
            token_hashed=hashed_token,
            token_expires_on=datetime.datetime.now() + datetime.timedelta(minutes=15),
            is_active=False
        )
        create_user(db, user)
    except Exception as e:
        raise HTTPException(
            status_code=400,
            detail="User, email or phone in use"
        )

    if request.client.host == 'testclient':
        return {"status": "DEBUG", "message": "Testing without sending.", "token": plain_token}
    else:
        try:
            port = os.environ.get('SMTP_PORT', 465)
            sender = os.environ.get('SMTP_USER', "johndoe@example.com")
            server = os.environ.get('SMTP_URI', "smtp.example.com")
            server_from = os.environ.get('SERVER_NAME', "http://localhost")
            password = os.environ.get('SMTP_PASSWORD', "test")
            message = MIMEText("Your code for " + server_from + " is " + ("%06d" % r))
            message['Subject'] = "Your verification code"
            message['From'] = sender
            message['To'] = reporter.email
            with smtplib.SMTP_SSL(server, port) as server:
                server.login(sender, password)
                server.sendmail(sender, reporter.email, message.as_string())
                return {"status": "OK", "message": "Mail sent"}
        except Exception as e:
            raise HTTPException(
                status_code=500,
                detail="Mail could not be sent. Please contact support"
            )


@router.post("/api/v1/verify")
def post_verification(verification: Verification, db: Session = Depends(get_db)):
    """When receiving an email, send a token to the email address and wait for the return."""
    try:
        user: DbUser = get_user_by_email(db, verification.email)
        if check_token(user.token_hashed, verification.token):
            user.token_hashed = None
            user.token_expires_on = None
            user.is_active = True
            user.session_token = secrets.token_hex()
            db.commit()
            return JSONResponse(
                content={"status": "OK", "message": "Token verified"},
                headers={"X-session-token": user.session_token}
            )
        else:
            raise HTTPException(
                status_code=500,
                detail="Token not verified"
            )

    except Exception as e:
        print(e)
        raise HTTPException(
            status_code=400,
            detail="User does not exist"
        )

