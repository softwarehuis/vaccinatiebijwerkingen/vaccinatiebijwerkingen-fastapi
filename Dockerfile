FROM python:3.10-slim
# Python cannot be upgraded without fixing a libgeos error

WORKDIR /usr/src/app

LABEL maintainer="Milo van der Linden - https://www.softwarehuis.org"
ENV BACKEND_ORIGINS "http://localhost,http://localhost:4200,http://localhost:3000,https://vaccinatiebijwerkingen.nl"
ENV SERVER_NAME localhost
ENV SMTP_PORT 465
ENV SMTP_URI smtp.example.com
ENV SMTP_USER johndoe@example.com
ENV SMTP_PASSWORD test

COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

COPY . .

EXPOSE 8000
VOLUME ["/usr/src/app/persist"]
CMD [ "python", "./service.py" ]