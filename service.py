import uvicorn
from src import api


if __name__ == "__main__":
    """
    The main entry point for the API, runs a uvicorn server on port 8000.
    """
    uvicorn.run(api, host="0.0.0.0", port=8000)
